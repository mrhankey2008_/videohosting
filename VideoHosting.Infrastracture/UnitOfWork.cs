﻿using System;
using System.Data;
using System.Data.Entity;

namespace VideoHosting.Infrastracture
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContextTransaction _transaction;

        private bool _commited;

        private DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
            _transaction = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
                _context.SaveChanges();
                _commited = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Dispose()
        {
            if (!_commited)
            {
                _transaction.Rollback();
            }
        }
    }
}