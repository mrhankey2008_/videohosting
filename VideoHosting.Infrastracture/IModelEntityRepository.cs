﻿namespace VideoHosting.Infrastracture
{
    public interface IModelEntityRepository<TItem> : IRepository<TItem>
        where TItem : class, IModelEntity
    {

    }
}