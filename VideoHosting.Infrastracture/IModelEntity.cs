﻿using System;

namespace VideoHosting.Infrastracture
{
    public interface IModelEntity
    {
        int Id { get; set; }

        DateTime Timestamp { get; set; }
    }
}