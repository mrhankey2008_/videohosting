﻿using System.Data.Entity;

namespace VideoHosting.Infrastracture
{
    public class ModelRepository<TModelItem> : RepositoryBase<TModelItem>, IModelEntityRepository<TModelItem>
        where TModelItem : class, IModelEntity
    {
        public ModelRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
