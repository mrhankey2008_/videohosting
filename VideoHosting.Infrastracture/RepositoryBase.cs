﻿using System;
using System.Data.Entity;
using System.Linq;

namespace VideoHosting.Infrastracture
{
    public class RepositoryBase<TItem> : IRepository<TItem>
        where TItem : class
    {
        public IQueryable<TItem> Items
        {
            get { return _dbSet; }
        }

        protected IDbSet<TItem> _dbSet;

        protected DbContext _context;

        public RepositoryBase(DbContext dbContext)
        {
            _context = dbContext;
            _dbSet = dbContext.Set<TItem>();
        }

        public virtual TItem AddItem(TItem item)
        {
            return _dbSet.Add(item);
        }

        public virtual TItem UpdateItem(TItem item)
        {
            return item;
        }

        public virtual TItem Remove(TItem item)
        {
            return _dbSet.Remove(item);
        }

        public virtual void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}