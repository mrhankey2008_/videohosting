﻿using System.Data.Entity;

namespace VideoHosting.Infrastracture
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private DbContext _context;

        public UnitOfWorkFactory(DbContext context)
        {
            _context = context;
        }

        public IUnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork(_context);
        }
    }
}