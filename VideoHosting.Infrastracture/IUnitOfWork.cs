﻿using System;

namespace VideoHosting.Infrastracture
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}