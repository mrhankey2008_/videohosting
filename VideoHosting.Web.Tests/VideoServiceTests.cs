﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using VideoHosting.Infrastracture;
using VideoHosting.VideoContext.Models;
using VideoHosting.Web.Mapper;
using VideoHosting.Web.Services;
using VideoHosting.Web.WebApiContracts;

namespace VideoHosting.Web.Tests
{
    [TestFixture]
    public class VideoServiceTests
    {
        [Test]
        public void ShouldBeGetVideoContractByVideoId()
        {
            var videoContract = new VideoContract
            {
                Id = 1,
                Timestamp = DateTime.MinValue,
                Description = "Description",
                FileName = "filename.mp4",
                Title = "Title"
            };

            var videoItem = new Video
            {
                Id = 1,
                Timestamp = DateTime.MinValue,
                Description = "Description",
                FileName = "filename.mp4",
                Title = "Title"
            }; 

            List<Video> videos = new List<Video>
            {
                videoItem
            };

            IQueryable<Video> queryableVideos = videos.AsQueryable();

            var repositoryMock = new Mock<IModelEntityRepository<Video>>();
            repositoryMock.Setup(x => x.Items).Returns(queryableVideos);

            var contractMapperMock = new Mock<IContractMapper>();
            contractMapperMock.Setup(x => x.Map<Video, VideoContract>(videoItem)).Returns(videoContract);

            var videoService = new VideoService(contractMapperMock.Object,
                Mock.Of<IUnitOfWorkFactory>(),
                repositoryMock.Object);

            var videoContractResult = videoService.Get(1);

            Assert.IsNotNull(videoContractResult);
            Assert.AreEqual(videoContract, videoContractResult);
        }

        [Test]
        public void ShouldBeThrowWebApiNotFoundExceptionWhenGetVideoContractByVideoId()
        {
            var videoService = new VideoService(Mock.Of<IContractMapper>(),
                Mock.Of<IUnitOfWorkFactory>(),
                Mock.Of<IModelEntityRepository<Video>>());

            var action = new Func<VideoContract>(() => videoService.Get(2));

            action.Should().Throw<WebApiNotFoundException>();
        }

        [Test]
        public void ShouldBeListVideoContracts()
        {
            List<VideoContract> videoContracts = new List<VideoContract>
            {
                TestData.VideoContract,
                TestData.VideoContract2
            };

            List<Video> videos = new List<Video>
            {
                TestData.Video,
                TestData.Video2
            };

            IQueryable<Video> queryableVideos = videos.AsQueryable();

            var repositoryMock = new Mock<IModelEntityRepository<Video>>();
            repositoryMock.Setup(x => x.Items).Returns(queryableVideos);

            var contractMapperMock = new Mock<IContractMapper>();
            contractMapperMock.Setup(x => x.Map<List<Video>, List<VideoContract>>(videos)).Returns(videoContracts);

            var videoService = new VideoService(contractMapperMock.Object,
                Mock.Of<IUnitOfWorkFactory>(),
                repositoryMock.Object);

            var videoContractsResult = videoService.List();

            Assert.IsNotNull(videoContractsResult);
            Assert.AreEqual(2, videoContractsResult.Count());
            Assert.AreEqual(videoContractsResult, videoContracts);
        }

        [Test]
        public void ShouldBeAddVideoInRepositoryWhichCorrespondsToInputVideoContract()
        {
            var videoContract = TestData.VideoContract;

            var addVideoContract = TestData.AddVideoContract;

            var videoItem = TestData.Video;

            var repositoryMock = new Mock<IModelEntityRepository<Video>>();

            var contractMapperMock = new Mock<IContractMapper>();
            contractMapperMock.Setup(x => x.Map<AddVideoContract, Video>(addVideoContract)).Returns(videoItem);
            contractMapperMock.Setup(x => x.Map<Video, VideoContract>(videoItem)).Returns(videoContract);

            var unitOfWork = new Mock<IUnitOfWork>();
            var unitOfWorkFactoryMock = new Mock<IUnitOfWorkFactory>();
            unitOfWorkFactoryMock.Setup(x => x.CreateUnitOfWork()).Returns(unitOfWork.Object);

            var videoService = new VideoService(contractMapperMock.Object,
                unitOfWorkFactoryMock.Object,
                repositoryMock.Object);

            videoService.Add(addVideoContract);

            repositoryMock.Verify(x => x.AddItem(videoItem));
        }

    }
    public class TestData
    {
        public static VideoContract VideoContract { get; set; }

        public static VideoContract VideoContract2 { get; set; }

        public static AddVideoContract AddVideoContract { get; set; }

        public static Video Video { get; set; }

        public static Video Video2 { get; set; }

        public TestData()
        {
            VideoContract = new VideoContract
            {
                Id = 1,
                Timestamp = DateTime.MinValue,
                Description = "Description",
                FileName = "filename.mp4",
                Title = "Title"
            };

            VideoContract2 = new VideoContract
            {
                Id = 2,
                Timestamp = DateTime.MinValue,
                Description = "Description2",
                FileName = "filename2.mp4",
                Title = "Title2"
            };

            AddVideoContract = new AddVideoContract
            {
                Description = "Description",
                FileName = "filename.mp4",
                Title = "Title"
            };

            Video = new Video
            {
                Id = 1,
                Timestamp = DateTime.MinValue,
                Description = "Description",
                FileName = "filename.mp4",
                Title = "Title"
            };

            Video2 = new Video
            {
                Id = 2,
                Timestamp = DateTime.MinValue,
                Description = "Description2",
                FileName = "filename2.mp4",
                Title = "Title2"
            };

        }
    }

}
