﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using VideoHosting.Web.Services;

namespace VideoHosting.Web.Controllers
{
    public class StreamingApiController : ApiController
    {
        private readonly IVideoStreamService _videoStreamService;

        public StreamingApiController(IVideoStreamService videoStreamService)
        {
            _videoStreamService = videoStreamService ?? throw new ArgumentNullException(nameof(videoStreamService));
        }

        [HttpGet]
        [Route("api/Stream/Play", Name = "PlayVideo")]
        public HttpResponseMessage Play(string filename)
        {
            var ext = Path.GetExtension(filename);
            _videoStreamService.SetFileName(filename);

            var response = Request.CreateResponse();
            response.Content = new PushStreamContent((Action<Stream, HttpContent, TransportContext>)_videoStreamService.WriteToStream,
                new MediaTypeHeaderValue("video/" + ext));

            return response;
        }
    }
}