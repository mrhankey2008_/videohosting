﻿using System.Web.Mvc;

namespace VideoHosting.Web.Controllers
{
    public class VideosController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Watch(int videoId)
        {
            ViewBag.VideoId = videoId;
            return View();
        }
	}
}