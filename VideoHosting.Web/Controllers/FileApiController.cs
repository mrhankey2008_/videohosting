﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VideoHosting.Web.Models;
using VideoHosting.Web.Services;

namespace VideoHosting.Web.Controllers
{
    public class FileApiController : ApiController
    {
        private readonly IFileService _fileService;

        public FileApiController(IFileService fileService)
        {
            _fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
        }

        [HttpPost]
        [Route("api/File/UploadFile", Name="UploadFile")]
        public async Task<UploadedFile> UploadFile(string fileName)
        {
            var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

            var httpContent = filesReadToProvider.Contents.FirstOrDefault();

            if (httpContent == null)
            {
                throw new WebApiException("HttpContent is null!");
            }

            var fileBytes = await httpContent.ReadAsByteArrayAsync();
            return await _fileService.Upload(fileBytes, fileName);
        }

    }
}