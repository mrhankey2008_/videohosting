﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using VideoHosting.Web.Services;
using VideoHosting.Web.WebApiContracts;

namespace VideoHosting.Web.Controllers
{
    
    public class VideosApiController : ApiController
    {
        private readonly IVideoService _videoService;

        public VideosApiController(IVideoService videoService)
        {
            _videoService = videoService ?? throw new ArgumentNullException(nameof(videoService));
        }

        [Route("api/Videos/Delete/{id}")]
        [HttpDelete]
        public VideoContract Delete(int id)
        {
            return _videoService.Delete(id);
        }

        [Route("api/Videos/Get/{id}", Name="GetVideo")]
        [HttpGet]
        public VideoContract Get(int id)
        {
            return _videoService.Get(id);
        }

        [Route("api/Videos/Add", Name="AddVideo")]
        [HttpPost]
        public VideoContract Add([FromBody] AddVideoContract video)
        {
            return _videoService.Add(video);
        }

        [Route("api/Videos/Update/{id}")]
        [HttpPut]
        public VideoContract Update(int videoId, [FromBody] EditVideoContract video)
        {
            return _videoService.Update(videoId, video);
        }

        [Route("api/Videos/Get", Name="GetAllVideo")]
        [HttpGet]
        public List<VideoContract> List()
        {
            return _videoService.List();
        }
    }
}