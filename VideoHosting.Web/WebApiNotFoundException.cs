﻿namespace VideoHosting.Web
{
    public class WebApiNotFoundException : WebApiException
    {
        public int ItemId { get; private set; }

        public WebApiNotFoundException(string message, int itemId)
            : base(message)
        {
            UserFriendlyMessage = message;
            ItemId = itemId;
        }
    }
}