﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using VideoHosting.Web.Models;

namespace VideoHosting.Web
{
    public class LogActionHandlerAttribute : ActionFilterAttribute
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public override void OnActionExecuting(HttpActionContext context)
        {
            Log("OnActionExecuting", context);
        }

        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            Log("OnActionExecuted", context);
        }

        private void Log<T>(string methodName, T filterContext)
        {
            switch (methodName)
            {
                case "OnActionExecuting":
                    {
                        var actionExecutingContext = filterContext as HttpActionContext;
                        if (!actionExecutingContext.ModelState.IsValid)
                        {
                            var errorDetails = GetErrorMessage(actionExecutingContext.ModelState);
                            var errorContract = new Error
                            {
                                ErrorMessage = "Model validation error",
                                ErrorDetails = errorDetails
                            };
                            actionExecutingContext.Response = actionExecutingContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorContract);
                            actionExecutingContext.Response.StatusCode = HttpStatusCode.BadRequest;
                        }
                        break;
                    }
                case "OnActionExecuted":
                    {
                        var actionExecutedContext = filterContext as HttpActionExecutedContext;
                        if (actionExecutedContext.Exception != null)
                        {
                            if (actionExecutedContext.Exception is WebApiException)
                            {
                                var message = ((WebApiException)actionExecutedContext.Exception).UserFriendlyMessage;
                                var errorContract = new Error
                                {
                                    ErrorMessage = message
                                };
                                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorContract);
                                actionExecutedContext.Response.StatusCode = GetStatusCode(actionExecutedContext.Exception);
                            }
                            LoggingInformation(methodName, actionExecutedContext, "ERROR");
                        }
                        else
                        {
                            LoggingInformation(methodName, actionExecutedContext, "TRACE");
                        }
                        break;
                    }
            }
        }

        private void LoggingInformation(string methodName, HttpActionExecutedContext context, string level)
        {

            var action = context.ActionContext.ActionDescriptor.ActionName;
            var controller = context.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;

            var message = level.Contains("ERROR") ? GetErrorMessage(context) : String.Format("Successfully! {0} controller:{1} action:{2}", methodName, controller, action);

            if (level.Contains("ERROR"))
            {
                _logger.Error(message);
            }
            else
            {
                _logger.Info(message);
            }
        }

        private static string GetErrorMessage(HttpActionExecutedContext filterContext)
        {
            var exception = filterContext.Exception;
            var message = string.Format("Exception: {0}\r\n Stack trace: {1}",
                exception.Message,
                exception.StackTrace
            );

            exception = exception.InnerException;
            while (exception != null)
            {
                message += "\r\n Inner exception:\r\n" + exception.Message;
                exception = exception.InnerException;
            }

            return message;
        }

        private static Dictionary<string, string[]> GetErrorMessage(ModelStateDictionary modelState)
        {
            return modelState
                .Where(e => !string.IsNullOrEmpty(e.Key))
                .ToDictionary(e => e.Key, e =>
                {
                    return e.Value.Errors?.Select(er => er.ErrorMessage).ToArray();
                });
        }

        private static HttpStatusCode GetStatusCode(Exception exception)
        {
            if (exception is WebApiNotFoundException)
            {
                return HttpStatusCode.NotFound;
            }
            if (exception is WebApiException)
            {
                return HttpStatusCode.BadRequest;
            }
            return HttpStatusCode.InternalServerError;
        }

    }
}