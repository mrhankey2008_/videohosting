﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VideoHosting.Web.WebApiContracts
{
    public class VideoContract
    {
        public int Id { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        public string FileName { get; set; }
    }
}