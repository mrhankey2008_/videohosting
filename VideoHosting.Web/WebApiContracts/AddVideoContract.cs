﻿using System.ComponentModel.DataAnnotations;

namespace VideoHosting.Web.WebApiContracts
{
    public class AddVideoContract
    {
        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        public string FileName { get; set; }
    }
}