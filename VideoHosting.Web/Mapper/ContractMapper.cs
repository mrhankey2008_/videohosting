﻿using AutoMapper;

namespace VideoHosting.Web.Mapper
{
    internal sealed class ContractMapper : IContractMapper
    {
        private static IMapper _mapper;

        public ContractMapper()
        {
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<VideoProfile>();
            });
            _mapper = configurationProvider.CreateMapper();
        }

        public TItem2 Map<TItem1, TItem2>(TItem1 source)
        {
            return _mapper.Map<TItem1, TItem2>(source);
        }

        public void Map<TItem1, TItem2>(TItem1 source, TItem2 destination)
        {
            _mapper.Map(source, destination);
        }
    }
}