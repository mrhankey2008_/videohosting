﻿namespace VideoHosting.Web.Mapper
{
    public interface IContractMapper
    {
        TItem2 Map<TItem1, TItem2>(TItem1 source);
        void Map<TItem1, TItem2>(TItem1 source, TItem2 destination);
    }
}
