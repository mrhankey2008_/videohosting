﻿using AutoMapper;
using VideoHosting.VideoContext.Models;
using VideoHosting.Web.WebApiContracts;

namespace VideoHosting.Web.Mapper
{
    public class VideoProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Video, VideoContract>();
            CreateMap<AddVideoContract, Video>();
            CreateMap<EditVideoContract, Video>();
        }
    }
}