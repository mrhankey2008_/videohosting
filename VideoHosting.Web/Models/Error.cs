﻿using System.Collections.Generic;

namespace VideoHosting.Web.Models
{
    public class Error
    {
        public string ErrorMessage { get; set; }

        public Dictionary<string, string[]> ErrorDetails { get; set; }

        public Error()
        {
            ErrorDetails = new Dictionary<string, string[]>();
        }
    }
}