﻿namespace VideoHosting.Web.Models
{
    public class UploadedFile
    {
        public bool IsUploaded { get; set; }

        public string FileName { get; set; }
    }
}