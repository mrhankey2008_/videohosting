﻿$(document).ready(function () {
    $('#title').prop('required', true);
    $('#uploadFile').prop('required', true);

    $('#title').bind("change", changeTitle);
    $('#uploadFile').bind("change", changeFile);

    getAllVideos();

});

function changeTitle() {
    if ($('#title').val()) {
        $('#title').prop('required', false);
    }
    if ($('#title').val() === '') {
        $('#title').prop('required', true);
    }
}

function changeFile() {
    if ($('#uploadFile').val()) {
        $('#uploadFile').prop('required', false);
    }
}

function getAllVideos() {
    $.ajax({
        type: "GET",
        url: window.urlGetAllVideos,
        success: function (data) {
            $.each(data,
                function (key, item) {
                    $('<a>',
                        {
                            text: item.Title,
                            href: window.urlWatchVideo + '?videoId=' + item.Id,
                            class: 'list-group-item list-group-item-action'
                        }).appendTo($('#videos'));
                });
        },
        error: function (status) {
            alert(status);
        }
    });
}

function addVideo(fileName) {
    var video = new Object();
    video.Title = $('#title').val();
    video.FileName = fileName;
    video.Description = $('#description').val();

    $.ajax({
        type: "POST",
        url: window.urlAddVideo,
        data: video,
        success: function (addedVideo) {
            updateListVideos(addedVideo);
            resetFields();
        },
        error: function () {
            alert(status);
        }
    });
}

function resetFields() {
    $('#title').val("");
    $('#description').val("");
    $('#uploadFile').val("");
    $('#title').prop('required', true);
    $('#uploadFile').prop('required', true);
}

function updateListVideos(addedVideo) {
    $('<a>',
        {
            text: addedVideo.Title,
            href: window.urlWatchVideo + '?videoId=' + addedVideo.Id,
            class: 'list-group-item list-group-item-action'
        }).appendTo($('#videos'));
}

function uploadFile() {
    var isTitleRequired = $('#title').prop('required');
    var isUploadFileRequired = $('#uploadFile').prop('required');

    if (isTitleRequired || isUploadFileRequired) {
        var errorMessage = isTitleRequired ? "Title field required!\n" : "";
        errorMessage += isUploadFileRequired ? "File field required!" : "";
        alert(errorMessage);
        return;
    }
    showPleaseWait();
    var uploadingFiles = $('#uploadFile')[0].files;
    if (uploadingFiles.length > 0) {
        var uploadingFile = uploadingFiles[0];
        var formData = new FormData();
        formData.append('uploadedFile', uploadingFile, uploadingFile.name);
        $.ajax({
            type: "POST",
            url: window.urlUploadVideo + "?fileName=" + uploadingFile.name,
            contentType: false,
            processData: false,
            data: formData,
            success: function (uploadedFile) {
                if (uploadedFile.IsUploaded) {
                    addVideo(uploadedFile.FileName);
                    hidePleaseWait();
                }
            },
            error: function (status) {
                alert(status);
                hidePleaseWait();
            }
        });
    } else {
        alert('Not selected file!');
    }
}

function showPleaseWait() {
    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false role="dialog">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-header">\
                    <h4 class="modal-title">Please wait...</h4>\
                </div>\
                <div class="modal-body">\
                    <div class="progress">\
                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                      </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $('#pleaseWaitDialog').modal({
        keyboard: false
    });
    $("#pleaseWaitDialog").modal("show");
}

function hidePleaseWait() {
    $("#pleaseWaitDialog").modal("hide");
}