﻿using System;

namespace VideoHosting.Web
{
    public class WebApiException : Exception
    {
        public string UserFriendlyMessage { get; protected set; }

        public WebApiException(string message, string userFriendlyMessage)
            : base(message)
        {
            UserFriendlyMessage = userFriendlyMessage;
        }

        public WebApiException(string message)
            : base(message)
        {
            UserFriendlyMessage = message;
        }
    }
}