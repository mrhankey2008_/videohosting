﻿using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using VideoHosting.Infrastracture;
using VideoHosting.VideoContext.Models;
using VideoHosting.Web.Mapper;
using VideoHosting.Web.Services;

namespace VideoHosting.Web
{
    internal static class ContainerFactory
    {
        public static void ResolveContainer()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;

            var assembly = Assembly.Load("VideoHosting.Web");

            builder.RegisterControllers(assembly);

            builder.RegisterApiControllers(assembly);

            var dbConnectionString = ConfigurationManager.ConnectionStrings["VideoHostingDb"].ConnectionString;

            builder.Register(x => new VideoContext.VideoContext(dbConnectionString)).InstancePerLifetimeScope(); ;

            builder.Register(x => new UnitOfWorkFactory(x.Resolve<VideoContext.VideoContext>()))
                .As<IUnitOfWorkFactory>().InstancePerLifetimeScope(); 

            builder.RegisterType<ContractMapper>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            builder.Register(CreateModelRepository<Video>);

            builder.Register(x => new VideoService(
                    x.Resolve<IContractMapper>(),
                    x.Resolve<IUnitOfWorkFactory>(),
                    x.Resolve<IModelEntityRepository<Video>>()))
                .As<IVideoService>();

            var directoryFiles = ConfigurationManager.AppSettings["directoryFiles"];

            builder.Register(x => new FileService(directoryFiles))
                .As<IFileService>()
                .AsSelf();

            builder.Register(x => new VideoStreamService(directoryFiles))
                .As<IVideoStreamService>()
                .AsSelf();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private static IModelEntityRepository<T> CreateModelRepository<T>(IComponentContext provider)
            where T : class, IModelEntity
        {
            return new ModelRepository<T>(provider.Resolve<VideoContext.VideoContext>());
        }

    }
}