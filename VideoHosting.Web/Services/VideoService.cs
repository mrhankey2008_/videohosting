﻿using System;
using System.Collections.Generic;
using System.Linq;
using VideoHosting.Infrastracture;
using VideoHosting.VideoContext.Models;
using VideoHosting.Web.Mapper;
using VideoHosting.Web.WebApiContracts;

namespace VideoHosting.Web.Services
{
    public class VideoService : IVideoService
    {
        private readonly IContractMapper _mapper;
        private readonly IModelEntityRepository<Video> _repository;
        private readonly IUnitOfWorkFactory _uofFactory;

        public VideoService(IContractMapper mapper,
            IUnitOfWorkFactory uofFactory,
            IModelEntityRepository<Video> repository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _uofFactory = uofFactory ?? throw new ArgumentNullException(nameof(uofFactory));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public VideoContract Get(int videoId)
        {
            var video = _repository.Items.FirstOrDefault(v => v.Id == videoId);

            if (video == null)
            {
                throw new WebApiNotFoundException($"Video with id = {videoId} is not found", videoId);
            }
            var inviteContract = _mapper.Map<Video, VideoContract>(video);
            return inviteContract;
        }

        public VideoContract Add(AddVideoContract contract)
        {
            using (var uof = _uofFactory.CreateUnitOfWork())
            {
                var video = _mapper.Map<AddVideoContract, Video>(contract);
                _repository.AddItem(video);

                uof.Commit();
                return _mapper.Map<Video, VideoContract>(video);
            }
        }

        public VideoContract Update(int videoId, EditVideoContract contract)
        {
            var video = _repository.Items
                .FirstOrDefault(m => m.Id == videoId);

            if (video == null)
            {
                throw new WebApiNotFoundException($"Video with id = {videoId} is not found", videoId);
            }

            _mapper.Map(contract, video);
            using (var uof = _uofFactory.CreateUnitOfWork())
            {
                _repository.UpdateItem(video);
                uof.Commit();
            }

            return _mapper.Map<Video, VideoContract>(video);
        }

        public VideoContract Delete(int videoId)
        {
            var video = _repository.Items.FirstOrDefault(inv => inv.Id == videoId);

            if (video == null)
            {
                throw new WebApiNotFoundException($"Video with id = {videoId} is not found", videoId);
            }
            using (var uof = _uofFactory.CreateUnitOfWork())
            {
                _repository.Remove(video);
                uof.Commit();
                var videoContract = _mapper.Map<Video, VideoContract>(video);
                return videoContract;
            }
        }

        public List<VideoContract> List()
        {
            var list = _repository.Items.ToList();
            return _mapper.Map<List<Video>, List<VideoContract>>(list);
        }
    }
}