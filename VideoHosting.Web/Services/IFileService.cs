﻿using System.Threading.Tasks;
using VideoHosting.Web.Models;

namespace VideoHosting.Web.Services
{
    public interface IFileService
    {
        Task<UploadedFile> Upload(byte[] bytesFile, string fileName);
    }
}
