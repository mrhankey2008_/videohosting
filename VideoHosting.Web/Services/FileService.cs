﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;
using VideoHosting.Web.Models;

namespace VideoHosting.Web.Services
{
    public sealed class FileService : IFileService
    {
        private readonly string _directoryFiles;
        public FileService(string directoryFiles)
        {
            _directoryFiles = directoryFiles ?? throw new ArgumentNullException(nameof(directoryFiles));
        }

        public async Task<UploadedFile> Upload(byte[] bytesFile, string fileName)
        {
            try
            {
                string pathDirectoryFiles = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, _directoryFiles);

                if (!Directory.Exists(pathDirectoryFiles))
                {
                    Directory.CreateDirectory(pathDirectoryFiles);
                }

                var fileInfo = MakeUniqueFileName(Path.Combine(pathDirectoryFiles, fileName));

                using (var fileStream = File.Create(fileInfo.FullName))
                {
                    await fileStream.WriteAsync(bytesFile, 0, bytesFile.Length);
                }

                return new UploadedFile
                {
                    IsUploaded = true,
                    FileName = fileInfo.Name
                };
            }
            catch (Exception e)
            {
                return new UploadedFile
                {
                    IsUploaded = false,
                    FileName = null
                };
            }
        }

        private FileInfo MakeUniqueFileName(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new NullReferenceException((nameof(path)));
            string directoryName = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(directoryName)) throw new NullReferenceException((nameof(directoryName)));
            string fileName = Path.GetFileNameWithoutExtension(path);
            if (string.IsNullOrEmpty(fileName)) throw new NullReferenceException((nameof(fileName)));
            string fileExt = Path.GetExtension(path);
            if (string.IsNullOrEmpty(fileExt)) throw new NullReferenceException((nameof(fileExt)));

            for (int i = 1; ; ++i)
            {
                if (!File.Exists(path))
                {
                    return new FileInfo(path);
                }
                path = Path.Combine(directoryName, fileName + " " + i + fileExt);
            }
        }
    }
}