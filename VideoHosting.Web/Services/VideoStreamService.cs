﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;

namespace VideoHosting.Web.Services
{
    public class VideoStreamService : IVideoStreamService
    {
        private readonly string _directoryFiles;

        private string _fileName;

        public VideoStreamService(string directoryFiles)
        {
            _directoryFiles = directoryFiles ?? throw new ArgumentNullException(nameof(directoryFiles));
        }

        public void SetFileName(string fileName)
        {
            _fileName = fileName;
        }

        public async void WriteToStream(Stream outputStream, HttpContent content, TransportContext context)
        {
            try
            {
                string pathDirectoryFiles = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, _directoryFiles);

                var pathFile = Path.Combine(pathDirectoryFiles, _fileName);

                var buffer = new byte[65536];
                if (File.Exists(pathFile))
                {
                    using (var video = File.Open(pathFile, FileMode.Open, FileAccess.Read))
                    {
                        var length = (int)video.Length;
                        var bytesRead = 1;

                        while (length > 0 && bytesRead > 0)
                        {
                            bytesRead = video.Read(buffer, 0, Math.Min(length, buffer.Length));
                            await outputStream.WriteAsync(buffer, 0, bytesRead);
                            length -= bytesRead;
                        }
                    }
                }
            }
            catch (HttpException ex)
            {
                return;
            }
            finally
            {
                outputStream.Close();
            }
        }
    }
}