﻿using System.IO;
using System.Net;
using System.Net.Http;

namespace VideoHosting.Web.Services
{
    public interface IVideoStreamService
    {
        void WriteToStream(Stream outputStream, HttpContent content, TransportContext context);
        void SetFileName(string fileName);
    }
}
