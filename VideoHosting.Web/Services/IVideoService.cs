﻿using System.Collections.Generic;
using VideoHosting.Web.WebApiContracts;

namespace VideoHosting.Web.Services
{
    public interface IVideoService
    {
        VideoContract Get(int videoId);
        VideoContract Add(AddVideoContract contract);
        VideoContract Update(int videoId, EditVideoContract contract);
        VideoContract Delete(int videoId);
        List<VideoContract> List();
    }
}
