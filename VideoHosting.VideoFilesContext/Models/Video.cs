﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VideoHosting.Infrastracture;

namespace VideoHosting.VideoContext.Models
{
    public class Video : IModelEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime Timestamp { get; set; }

        [StringLength(ModelConstrains.TitleLength)]
        public string Title { get; set; }

        [StringLength(ModelConstrains.TextLength)]
        public string Description { get; set; }

        [StringLength(ModelConstrains.TitleLength)]
        public string FileName { get; set; }

        public Video()
        {
            Timestamp = DateTime.UtcNow;
        }
    }
}
