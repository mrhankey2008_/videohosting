﻿using System.Data.Entity;
using VideoHosting.VideoContext.Models;

namespace VideoHosting.VideoContext
{
    public class VideoContext : DbContext
    {
        public DbSet<Video> Videos { get; set; }

        public VideoContext() 
            : base("VideoHostingDb")
        {
        }

        public VideoContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
        }
    }
}
